# barry-i3-default-settings

In Project Barry's infancy, I had originally considered having [Qtile](http://qtile.org) be the recommended window manager for the project, at least for the project when run on single screen e-readers, but was unable to get the 0.13.x release running in my development environent. As such, I had made the decision to try using [i3](https://i3wm.org/) instead.

This configuration includes a running header (using i3bar for now, but will eventually use [Polybar](https://polybar.github.io/), however, the Project Barry running header (technically a status bar) doesn't need to run as part of the window manager itself; an external application can also be used, hopefully with the EMWH classification of a "panel".

Developers who want to incorporate components from the project into their ports/projects/packages aren't forced to use ~~Qtile~~ i3 for the window manager, however, as they can use that, a different window manager and/or panel configuration, or go without a window manager, depending on their use case.

There eventually may be configuration options, but they're not available for now. At this point, I'm mainly just experimenting with configuration parameters.

## About the branches
The primary difference between each GIT branch is a different Polybar configuration.
* `master` - Only repository for now

## Dependencies for this component
_(Refer to above section for status bar font requirements)_
* i3
* ~~Polybar~~

## Known issues
1. Polybar configs are not ready, so i3bar is being used temporarily.
